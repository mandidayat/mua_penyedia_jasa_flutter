# Mua Penyedia Saja


The same code used for both iOS and Android

## Installation
### Setup Flutter environment
Follow base on the operating system on which you are installing Flutter:

Mac OS: https://flutter.dev/docs/get-started/install/macos

Window: https://flutter.dev/docs/get-started/install/windows

Linux: https://flutter.dev/docs/get-started/install/linux

Setup an editor: https://flutter.dev/docs/get-started/editor

### Test drive
This section describes how to create a new Flutter app, run it, and experience “hot reload” after you make changes to the Fluxstore app.

Select your development tool of choice for writing, building, and running Flutter apps.

[Android Studio / IntelliJ](https://flutter.dev/docs/get-started/test-drive#androidstudio)

[Visual Studio Code](https://flutter.dev/docs/get-started/test-drive#androidstudio)

[Terminal & editor](https://flutter.dev/docs/get-started/test-drive#androidstudio)

#### Create the app
1. Open the IDE and select **Start a new Flutter project**.
2. Select **Flutter Application** as the project type. Then click **Next**.
3. Verify the **Flutter SDK** path specifies the SDK’s location (select **Install SDK**… if the text field is blank).
4. Enter a project name (for example, `myapp`). Then click Next.
5. Click **Finish**.
6. Wait for Android Studio to install the SDK and create the project.

**Tip**: The code for your app is in `lib/main.dart`. For a high-level description of what each code block does, see the comments at the top of that file.

#### Run the app
1. Locate the main Android Studio toolbar: Main IntelliJ toolbar
![toolbar](img/main-toolbar.png)
2. In the target selector, select an Android device for running the app. If none are listed as available, select **Tools> Android > AVD Manager** and create one there. For details, see Managing AVDs.
3. Click the run icon in the toolbar, or invoke the menu item **Run > Run**. 

After the app build completes, you’ll see the starter app on your device.

![starter-app](img/starter-app.png)

#### Try hot reload
Flutter offers a fast development cycle with Stateful Hot Reload, the ability to reload the code of a live running app without restarting or losing app state. Make a change to app source, tell your IDE or command-line tool that you want to hot reload, and see the change in your simulator, emulator, or device.

1. Open **lib/main.dart**.
2. Change the string
```
'You have pushed the button this many times'
```
to
```
'You have clicked the button this many times'
```
_**Important**_: Do not stop your app. Let your app run.

Save your changes: invoke **Save All**, or click **Hot Reload** offline_bolt.

You’ll see the updated string in the running app almost immediately.

