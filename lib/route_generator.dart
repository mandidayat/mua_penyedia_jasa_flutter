import 'package:flutter/material.dart';
import 'package:restaurant_rlutter_ui/src/pages/about.dart';
import 'package:restaurant_rlutter_ui/src/pages/login.dart';
import 'package:restaurant_rlutter_ui/src/pages/pages.dart';
import 'package:restaurant_rlutter_ui/src/pages/product.dart';
import 'package:restaurant_rlutter_ui/src/pages/signup.dart';
import 'package:restaurant_rlutter_ui/src/pages/splash_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case '/Debug':
//        return MaterialPageRoute(builder: (_) => DebugWidget(routeArgument: args as RouteArgument));
      case '/Walkthrough':
//        return MaterialPageRoute(builder: (_) => Walkthrough());
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/SignUp':
        return MaterialPageRoute(builder: (_) => SignUpWidget());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginWidget());
      case '/Product':
        return MaterialPageRoute(builder: (_) => AllProductWidget());
      case '/Pages':
        return MaterialPageRoute(builder: (_) => PagesTestWidget(currentTab: args));
      case '/About':
        return MaterialPageRoute(builder: (_) => AboutWidget());
      case '/Settings':
//        return MaterialPageRoute(builder: (_) => SettingsWidget());
      default:
        // If there is no such named route in the switch statement, e.g. /third
//        return MaterialPageRoute(builder: (_) => PagesTestWidget(currentTab: 2));
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
