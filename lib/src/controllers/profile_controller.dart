import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart' as repository;

class ProfileController extends ControllerMVC {
  User user = new User();
  GlobalKey<FormState> updateFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  ProfileController() {
    updateFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForUser();
  }

  void update() async {
    if (updateFormKey.currentState.validate()) {
      updateFormKey.currentState.save();
      repository.update(user).then((value) {
        print(value);
        if (value != null) {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Success"),
          ));
          Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 0);
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(S.current.wrong_email_or_password),
          ));
        }
      });
    }
  }

  void listenForUser() {
    repository.getCurrentUser().then((_user) {
      setState(() {
        print("getUser");
        print(_user.nama);
        user = _user;
      });
    });
  }

  Future<void> refreshProfile() async {
//    recentOrders.clear();
    user = new User();
    listenForUser();
  }
}
