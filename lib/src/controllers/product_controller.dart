import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/src/models/model_all_product.dart';
import 'package:restaurant_rlutter_ui/src/repository/product_repository.dart';

class ProductController extends ControllerMVC {

  List<ModelAllProduct> product = <ModelAllProduct>[];

  GlobalKey<ScaffoldState> scaffoldKey;


  ProductController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForMyOrder();
  }

  void listenForMyOrder() async {
    getMyProduct().then((vale){
      if (vale != null) {
        setState(() {
          product = vale;
        });
      }
    });
  }


  Future<void> refreshOrders() async {
    product.clear();
    listenForMyOrder();
  }
}
