import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'package:restaurant_rlutter_ui/src/repository/order_today_repository.dart';

class HomeController extends ControllerMVC {
  List<DataLayanan> layanan = <DataLayanan>[];
  List<DataLayanan> duplicatlayanan = <DataLayanan>[];

  HomeController() {
    listenForOrderToday();
  }

  void listenForOrderToday() async {
    getOrderToday().then((vale){
      if (vale != null) {
        setState(() {
          layanan = vale.data;
          duplicatlayanan = vale.data;
        });
      }
    });
  }

  void listenForCancelOrder(String id) async {
    cancelOrder(id).then((vale){
      if (vale != null) {
        setState(() {
          refreshOrdersToday();
        });
      }
    });
  }

  Future<void> refreshOrdersToday() async {
    layanan.clear();
    listenForOrderToday();
  }

  void filterSearchResults(String query) {
    List<DataLayanan> dummyListlayanan = <DataLayanan>[];
    dummyListlayanan.addAll(duplicatlayanan);
    if(query.isNotEmpty) {
      List<DataLayanan> dummylayanan = <DataLayanan>[];
      dummyListlayanan.forEach((item) {
        if(item.namaLayanan.toLowerCase().contains(query.toLowerCase()) || item.nama.toLowerCase().contains(query.toLowerCase())) {
          dummylayanan.add(item);
        }
      });
      setState(() {
        layanan.clear();
        layanan.addAll(dummylayanan);
      });
      return;
    } else {
      setState(() {
        refreshOrdersToday();
      });
    }
  }

  Future<void> refreshHome() async {
    layanan.clear();
    listenForOrderToday();
  }
}
