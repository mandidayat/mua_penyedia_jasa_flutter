import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/src/models/model_my_order.dart';
import 'package:restaurant_rlutter_ui/src/repository/order_repository.dart';

class OrderController extends ControllerMVC {

  List<MyOrders> myOrder = <MyOrders>[];

  GlobalKey<ScaffoldState> scaffoldKey;

  OrderController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    listenForMyOrder();
  }


  void listenForMyOrder() async {
    getMyOrder().then((vale){
      if (vale != null) {
        setState(() {
          myOrder = vale;
        });
      }
    });
  }

  Future<void> refreshOrders() async {
    myOrder.clear();
    listenForMyOrder();
  }
}
