import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:intl/intl.dart' show DateFormat, NumberFormat;
import 'package:restaurant_rlutter_ui/src/models/model_my_order.dart';
import 'package:http/http.dart' as http;


class OrderItemWidget extends StatelessWidget {
  final String heroTag;
  final MyOrders myorder;

  const OrderItemWidget({Key key, this.myorder, this.heroTag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = new NumberFormat("#,###");

    return Container(
      width: 340,
      margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.5),
              blurRadius: 5,
              offset: Offset(0, 2)),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(getStatus(myorder.status)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 1),
            child: Container(
              color: Colors.black26,
              height: 1.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Nama Pemesan",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      myorder.namaCus,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    Text(
                      "No Telepon",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      myorder.noHpCus,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    SizedBox(height: 5),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Tanggal ",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      DateFormat('dd-MM-yyyy').format(myorder.tgl),
                      overflow: TextOverflow.fade,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    Text(
                      "Jam ",
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      myorder.jam_order,
                      overflow: TextOverflow.fade,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 1),
            child: Container(
              color: Colors.black26,
              height: 1.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Biaya",
                      overflow: TextOverflow.fade,
                    ),
                    Text(
                      "Rp. " + formatter.format(int.parse(myorder.harga)),
                      overflow: TextOverflow.fade,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Pemesanan Layanan",
                      overflow: TextOverflow.fade,
                    ),
                    Text(
                      myorder.namaLayanan,
                      overflow: TextOverflow.fade,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 1),
            child: Container(
              color: Colors.black26,
              height: 1.0,
            ),
          ),
          cekStatus(myorder.status,context),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//            child: Row(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              mainAxisAlignment: MainAxisAlignment.spaceAround,
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                Column(
//                  children: <Widget>[
//                    FlatButton(
//                      padding: EdgeInsets.all(8),
//                      onPressed: () {
//                        FutureBuilder(
//                          future: terimaLayanan(context),
//                          builder: (BuildContext context, AsyncSnapshot snapshot) {
//                            if (snapshot.hasData)
//                              return Center(
//                                child: Text("Succes"),
//                              );
//                            else
//                              return CircularProgressIndicator();
//                          },
//                        );
//                      },
//                      child: Text("Terima"),
//                      textColor: Colors.white,
//                      color: Colors.green,
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(5)),
//                    ),
//                  ],
//                ),
//                Column(
//                  children: <Widget>[
//                    FlatButton(
//                      padding: EdgeInsets.all(8),
//                      onPressed: () {
//                        FutureBuilder(
//                          future: tolakLayanan(context),
//                          builder: (BuildContext context, AsyncSnapshot snapshot) {
//                            if (snapshot.hasData)
//                              return Center(
//                                child: Text("Succes"),
//                              );
//                            else
//                              return CircularProgressIndicator();
//                          },
//                        );
//                      },
//                      child: Text("Tolak"),
//                      textColor: Colors.white,
//                      color: Colors.amber,
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(5)),
//                    ),
//                  ],
//                ),
//              ],
//            ),
//          ),
        ],
      ),
    );
  }

  Future tolakLayanan(BuildContext context) async {
    var bodya = {
      "id_order":myorder.id_order,
      "status" : "3",
    };

    final String url = '${GlobalConfiguration().getString('base_url')}order/order_update';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      Navigator.of(context).pushNamed('/Pages', arguments: 2);
    }
  }

  Future terimaLayanan(BuildContext context) async {
    var bodya = {
      "id_order":myorder.id_order,
      "status" : "2",
    };

    final String url = '${GlobalConfiguration().getString('base_url')}order/order_update';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      Navigator.of(context).pushNamed('/Pages', arguments: 2);
    }
  }

  Future selesaiLayanan(BuildContext context) async {
    var bodya = {
      "id_order":myorder.id_order,
      "status" : "4",
    };

    final String url = '${GlobalConfiguration().getString('base_url')}order/order_update';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      Navigator.of(context).pushNamed('/Pages', arguments: 2);
    }
  }

  cekStatus(String status, BuildContext context) {
    if (status == "1")
    {
      return  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {
                    FutureBuilder(
                      future: terimaLayanan(context),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData)
                          return Center(
                            child: Text("Succes"),
                          );
                        else
                          return CircularProgressIndicator();
                      },
                    );
                  },
                  child: Text("Terima"),
                  textColor: Colors.white,
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {
                    FutureBuilder(
                      future: tolakLayanan(context),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData)
                          return Center(
                            child: Text("Succes"),
                          );
                        else
                          return CircularProgressIndicator();
                      },
                    );
                  },
                  child: Text("Tolak"),
                  textColor: Colors.white,
                  color: Colors.amber,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ],
            ),
          ],
        ),
      );
    }
    else if (status == "2")
    {
      return  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {
                    FutureBuilder(
                      future: selesaiLayanan(context),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData)
                          return Center(
                            child: Text("Succes"),
                          );
                        else
                          return CircularProgressIndicator();
                      },
                    );
                  },
                  child: Text("Konfirmasi Selesai"),
                  textColor: Colors.white,
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ],
            ),
          ],
        ),
      );
    }
    else if (status == "3")
    {
      return  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {

                  },
                  child: Text("Di Tolak"),
                  textColor: Colors.white,
                  color: Colors.amber,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ],
            ),
          ],
        ),
      );
    }
    else if (status == "4")
    {
      return  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.all(8),
                  onPressed: () {

                  },
                  child: Text("Order Selesai"),
                  textColor: Colors.white,
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
              ],
            ),
          ],
        ),
      );
    }

  }

  
}


String getStatus(String status) {
  if (status == "1")
    return "Menunggu Confirmasi";
  else if (status == "2")
    return "Di Setujui";
  else if (status == "3")
    return "Di Tolak";
  else if (status == "4")
    return "Selesai";
}
