import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:restaurant_rlutter_ui/src/models/model_all_product.dart';

class ProductItemGrideWidget extends StatelessWidget {
  final String heroTag;
  final ModelAllProduct modelAllProduct;

  const ProductItemGrideWidget({Key key,  this.modelAllProduct, this.heroTag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
              color: Color(0xFF656565).withOpacity(0.15),
              blurRadius: 4.0,
              spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
            )
          ]),
      child: Wrap(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: mediaQueryData.size.height / 3.3,
                    width: 200.0,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(7.0),
                          topRight: Radius.circular(7.0)),
                        image: new DecorationImage(image: new NetworkImage('${GlobalConfiguration().getString('base_url_grocery')}uploads/products/'+modelAllProduct.productImage),
                            fit: BoxFit.cover)
                    ),
//                    decoration: BoxDecoration(
//                        borderRadius: BorderRadius.only(
//                            topLeft: Radius.circular(7.0),
//                            topRight: Radius.circular(7.0)),
//                        image: DecorationImage(
//                            image: AssetImage(modelAllProduct.productImage), fit: BoxFit.cover)),
                  ),
                  Container(
                    height: 25.5,
                    width: 105.0,
                    decoration: BoxDecoration(
                        color: Color(0xFFD7124A),
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(20.0),
                            topLeft: Radius.circular(5.0))),
                    child: Center(
                        child: Text(
                          getStatus(modelAllProduct.inStock),
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        )),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 7.0)),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: Text(
                  modelAllProduct.productName,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      letterSpacing: 0.5,
                      color: Colors.black54,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w500,
                      fontSize: 13.0),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 1.0)),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: Text(
                  'Rp. '+modelAllProduct.price,
                  style: TextStyle(
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w500,
                      fontSize: 14.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  String getStatus(String status) {
    if(status == "1")
      return "In Stock";
    else
      return "Out of Stock";

  }


}
