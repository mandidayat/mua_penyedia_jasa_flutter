import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/home_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/CircularLoadingWidget.dart';
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'CardOrderWidget.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:async/async.dart';

class CardsOrderTodayWidget extends StatefulWidget {

  List<DataLayanan> layanan;

  String heroTag;
  HomeController con;

  CardsOrderTodayWidget({Key key, this.layanan, this.heroTag,this.con}) : super(key: key);

  @override
  _CardsOrderTodayWidgetState createState() => _CardsOrderTodayWidgetState();
}

class _CardsOrderTodayWidgetState extends State<CardsOrderTodayWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.layanan.isEmpty
        ? CircularLoadingWidget(height: 288)
        : Container(
//            height: 1350,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: widget.layanan.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return DynamicDialog(namaLayanan: widget.con.layanan[index].namaLayanan, hargaLayanan: widget.con.layanan[index].harga,idLayanan: widget.con.layanan[index].idLayanan,idPenyediaJasa: widget.con.layanan[index].idPenyediaJasa,);
                      }),
                  child: CardOrderWidget(layanan: widget.layanan.elementAt(index), con: widget.con, heroTag: widget.heroTag),
                );
              },
            ),
          );
  }

}

class DynamicDialog extends StatefulWidget {
  String namaLayanan,hargaLayanan,idLayanan,idPenyediaJasa;

  DynamicDialog({this.namaLayanan, this.hargaLayanan,this.idLayanan, this.idPenyediaJasa});

  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  File images;
  GlobalKey<FormState> addProducFormKey;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      images = image;
    });
  }

  Future getImageCam() async {
    var image1 = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      images = image1;
    });
  }

  String nama_layanan,harga;

  Future getupdate() async {
    var bodya = {
      "nama_layanan":nama_layanan,
      "harga" : harga,
      "id_penyedia_jasa" : widget.idPenyediaJasa,
      "id_layanan" : widget.idLayanan,
    };

    final String url = '${GlobalConfiguration().getString('base_url')}layanan/update_layanan';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      HomeController f = new HomeController();
      f.refresh();
    }
  }

  Future getDelete() async {
    var bodya = {
      "id_layanan" : widget.idLayanan,
    };

    final String url = '${GlobalConfiguration().getString('base_url')}layanan/delete_layanan';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      HomeController f = new HomeController();
      f.refresh();
    }
  }


  Future deleteProduct(BuildContext context) async {
    if (addProducFormKey.currentState.validate()) {
      addProducFormKey.currentState.save();
      getDelete().then((value) {
        print(value);
      });

    }
  }

  Future editProduct(BuildContext context) async {
    if (addProducFormKey.currentState.validate()) {
      if(images != null){
        addProducFormKey.currentState.save();

        var stream = new http.ByteStream(DelegatingStream.typed(images.openRead()));
        // get file length
        var length = await images.length();

        Map<String, String> bod = {
          "nama_layanan":nama_layanan,
          "harga" : harga,
          "id_penyedia_jasa" : widget.idPenyediaJasa,
          "id_layanan" : widget.idLayanan,
        };

        final String url = '${GlobalConfiguration().getString('base_url')}layanan/update_layanan';
        var request = new http.MultipartRequest("POST", Uri.parse(url));
        var multipartFile = new http.MultipartFile('image', stream, length,
            filename: basename(images.path));

        request.files.add(multipartFile);
        request.fields.addAll(bod);

        var response = await request.send();
        var responseq = await response.stream.bytesToString();
        print(responseq);
        response.stream.transform(utf8.decoder).listen((value) {
          print("upload");
          print(value);
          setState(() {
            HomeController f = new HomeController();
            f.refresh();
          });

        });

      }else {
        print("upload no foto");
        addProducFormKey.currentState.save();
        getupdate().then((value) {
          print(value);
        });

      }
    }

  }

  @override
  void initState() {
    super.initState();
    addProducFormKey = new GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Edit Layanan")),
      content: Form(
        key: addProducFormKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  onSaved: (input) => nama_layanan = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  initialValue: widget.namaLayanan,
                  decoration: InputDecoration(
                    labelText: 'Nama Layanan',
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Nama Layanan',
                    labelStyle: TextStyle(color: Colors.black),
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  onSaved: (input) => harga = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  initialValue:widget.hargaLayanan,
                  decoration: InputDecoration(
                    labelText: 'Harga Layanan',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Harga Layanan',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Text("Gambar Layanan"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.image),
                    tooltip: 'Galery',
                    onPressed: () {
                      setState(() {
                        getImage();
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    tooltip: 'Camera',
                    onPressed: () {
                      setState(() {
                        getImageCam();
                      });
                    },
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
//                child: images == null
//                    ? Text('No image selected.')
//                    : Image.file(images),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: images == null
                          ? Text('No image selected.',)
                          : Image.file(images,
                          height: 150.0,
                          width: 200.0,
                          fit: BoxFit.cover
                      ),
                    ),

                    SizedBox(height: 5),
                  ],
                ),
              ),
              FlatButton(
                padding: EdgeInsets.all(8),
                onPressed: () {
                  setState(() {
                    if (addProducFormKey.currentState.validate()) {
                        editProduct(context);
                        Navigator.pop(context);
                        HomeController f = new HomeController();
                        f.refresh();
                        Navigator.of(context).pushNamed('/Pages', arguments: 1);
                    }
                  });
                },
                child: Text("Upload"),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
              FlatButton(
                padding: EdgeInsets.all(8),
                onPressed: () {
                  setState(() {
                    if (addProducFormKey.currentState.validate()) {
                      deleteProduct(context);
                      Navigator.pop(context);
                      HomeController f = new HomeController();
                      f.refresh();
                      Navigator.of(context).pushNamed('/Pages', arguments: 1);
                    }
                  });
                },
                child: Text("Hapus Layanan"),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


