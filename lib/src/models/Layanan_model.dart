import 'dart:convert';

LayananModel layananModelFromJson(String str) => LayananModel.fromJson(json.decode(str));

String layananModelToJson(LayananModel data) => json.encode(data.toJson());

class LayananModel {
  bool responce;
  List<DataLayanan> data;

  LayananModel({
    this.responce,
    this.data,
  });

  factory LayananModel.fromJson(Map<String, dynamic> json) => LayananModel(
    responce: json["responce"],
    data: List<DataLayanan>.from(json["data"].map((x) => DataLayanan.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "responce": responce,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataLayanan {
  String namaLayanan;
  String idLayanan;
  String harga;
  String foto;
  String idPenyediaJasa;
  String nama;

  DataLayanan({
    this.namaLayanan,
    this.idLayanan,
    this.harga,
    this.foto,
    this.idPenyediaJasa,
    this.nama,
  });

  factory DataLayanan.fromJson(Map<String, dynamic> json) => DataLayanan(
    namaLayanan: json["nama_layanan"],
    idLayanan: json["id_layanan"],
    harga: json["harga"],
    foto: json["foto"],
    idPenyediaJasa: json["id_penyedia_jasa"],
    nama: json["nama"],
  );

  Map<String, dynamic> toJson() => {
    "nama_layanan": namaLayanan,
    "id_layanan": idLayanan,
    "harga": harga,
    "foto": foto,
    "id_penyedia_jasa": idPenyediaJasa,
    "nama": nama,
  };
}
