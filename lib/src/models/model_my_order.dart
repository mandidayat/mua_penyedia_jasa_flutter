

import 'dart:convert';

  List<MyOrders> myOrdersFromJson(String str) => List<MyOrders>.from(json.decode(str)['data'].map((x) => MyOrders.fromJson(x)));

  String myOrdersToJson(List<MyOrders> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));


class MyOrders {
  String id_order;
  String jam_order;
  String status;
  DateTime tgl;
  String idCustomer;
  String namaCus;
  String alamatCus;
  String noHpCus;
  String nama;
  String alamat;
  String noHp;
  String namaLayanan;
  String harga;


  MyOrders({this.id_order, this.jam_order, this.status, this.tgl,
      this.idCustomer, this.namaCus, this.alamatCus, this.noHpCus, this.nama,
      this.alamat, this.noHp, this.namaLayanan, this.harga});


  factory MyOrders.fromJson(Map<String, dynamic> json) => MyOrders(
    id_order: json["id_order"],
    jam_order: json["jam_order"],
    status: json["status"],
    tgl: DateTime.parse(json["tgl"]),
    idCustomer: json["id_customer"],
    namaCus: json["namaCus"],
    alamatCus: json["alamatCus"],
    noHpCus: json["no_hpCus"],
    nama: json["nama"],
    alamat: json["alamat"],
    noHp: json["no_hp"],
    namaLayanan: json["nama_layanan"],
    harga: json["harga"],
  );

  Map<String, dynamic> toJson() => {
    "id_order": id_order,
    "jam_order": jam_order,
    "status": status,
    "tgl": "${tgl.year.toString().padLeft(4, '0')}-${tgl.month.toString().padLeft(2, '0')}-${tgl.day.toString().padLeft(2, '0')}",
    "id_customer": idCustomer,
    "namaCus": namaCus,
    "alamatCus": alamatCus,
    "no_hpCus": noHpCus,
    "nama": nama,
    "alamat": alamat,
    "no_hp": noHp,
    "nama_layanan": namaLayanan,
    "harga": harga,
  };

}