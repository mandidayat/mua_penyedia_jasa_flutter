
import 'dart:convert';

List<ModelAllProduct> modelAllProductFromJson(String str) => List<ModelAllProduct>.from(json.decode(str).map((x) => ModelAllProduct.fromJson(x)));

String modelAllProductToJson(List<ModelAllProduct> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelAllProduct {
  String productId;
  String productName;
  String productArbName;
  String productDescription;
  String productArbDescription;
  String productImage;
  String categoryId;
  String inStock;
  String price;
  String mrp;
  String unitValue;
  String unit;
  dynamic arbUnit;
  String increament;
  String rewards;
  String tax;
  String storeIdLogin;

  ModelAllProduct({
    this.productId,
    this.productName,
    this.productArbName,
    this.productDescription,
    this.productArbDescription,
    this.productImage,
    this.categoryId,
    this.inStock,
    this.price,
    this.mrp,
    this.unitValue,
    this.unit,
    this.arbUnit,
    this.increament,
    this.rewards,
    this.tax,
    this.storeIdLogin,
  });

  factory ModelAllProduct.fromJson(Map<String, dynamic> json) => ModelAllProduct(
    productId: json["product_id"],
    productName: json["product_name"],
    productArbName: json["product_arb_name"],
    productDescription: json["product_description"],
    productArbDescription: json["product_arb_description"],
    productImage: json["product_image"],
    categoryId: json["category_id"],
    inStock: json["in_stock"],
    price: json["price"],
    mrp: json["mrp"],
    unitValue: json["unit_value"],
    unit: json["unit"],
    arbUnit: json["arb_unit"],
    increament: json["increament"],
    rewards: json["rewards"],
    tax: json["tax"],
    storeIdLogin: json["store_id_login"],
  );

  Map<String, dynamic> toJson() => {
    "product_id": productId,
    "product_name": productName,
    "product_arb_name": productArbName,
    "product_description": productDescription,
    "product_arb_description": productArbDescription,
    "product_image": productImage,
    "category_id": categoryId,
    "in_stock": inStock,
    "price": price,
    "mrp": mrp,
    "unit_value": unitValue,
    "unit": unit,
    "arb_unit": arbUnit,
    "increament": increament,
    "rewards": rewards,
    "tax": tax,
    "store_id_login": storeIdLogin,
  };
}