
import 'dart:convert';

ModelUser modelUserFromJson(String str) => ModelUser.fromJson(json.decode(str));

String modelUserToJson(ModelUser data) => json.encode(data.toJson());

class ModelUser {
  bool responce;
  User user;

  ModelUser({
    this.responce,
    this.user,
  });

  factory ModelUser.fromJson(Map<String, dynamic> json) => ModelUser(
        responce: json["responce"],
        user: User.fromJSON(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responce": responce,
        "User": user.toJson(),
      };
}

class User {
  String idPenyediaJasa;
  String nama;
  String alamat;
  String noHp;
  String username;
  String password;

  User({
      this.idPenyediaJasa,
      this.nama,
      this.alamat,
      this.noHp,
      this.username,
      this.password
      });

  factory User.fromJSON(Map<String, dynamic> json) => User(
    idPenyediaJasa: json["id_penyedia_jasa"],
    nama: json["nama"],
    alamat: json["alamat"],
    noHp: json["no_hp"],
    username: json["username"],
    password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "idPenyediaJasa": idPenyediaJasa,
        "nama": nama,
        "alamat": alamat,
        "noHp": noHp,
        "username": username,
        "password": password,
      };

  toMap() {
    var bodya = {
      "username": username,
      "password": password,
    };
    return bodya;
  }
}
