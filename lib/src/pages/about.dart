import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/profile_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/BlockButtonWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/CircularLoadingWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/OrderItemWidget.dart';
import 'package:restaurant_rlutter_ui/src/elements/ProfileAvatarWidget.dart';

class AboutWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  AboutWidget({Key key, this.parentScaffoldKey}) : super(key: key);
  @override
  _AboutWidgetState createState() => _AboutWidgetState();
}

class _AboutWidgetState extends StateMVC<AboutWidget> {
  ProfileController _con;
  final TextEditingController _textEditingController = new TextEditingController();
  final TextEditingController _textEditingHPController = new TextEditingController();
  final TextEditingController _textEditingAddressController = new TextEditingController();

  _AboutWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
//          color: Theme.of(context).accentColor,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/img/logo.png'),
              SizedBox(height: 50),
              Text("Makeup Artist",style: Theme.of(context)
                  .textTheme
                  .title
                  .merge(TextStyle(letterSpacing: 1.3))),
              Text(S.of(context).version + " 1.4.1",style: Theme.of(context).textTheme.body1),
            ],
          ),
        ),
      ),
    );
  }
}
