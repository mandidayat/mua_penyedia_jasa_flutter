import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/home_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/CardsOrderTodayWidget.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:async/async.dart';

class HomeWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  HomeWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> {
  HomeController _con;
  TextEditingController editingController = TextEditingController();

  _HomeWidgetState() : super(HomeController()) {
    _con = controller;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'MakeUp Artist',
          style: Theme.of(context)
              .textTheme
              .title
              .merge(TextStyle(letterSpacing: 3.3)),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DynamicDialog();
                  });
            },
          ),
        ],
      ),
      body: new RefreshIndicator(
        onRefresh: _con.refreshHome,
        child: Container(
          child: Column(children: <Widget>[
          Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: TextField(
            onChanged: (value) {
              _con.filterSearchResults(value);
            },
            controller: editingController,
            decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                prefixIcon: Icon(Icons.search,color: Theme.of(context).accentColor),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4))),
          ),
        ),
          Expanded(
          child: CardsOrderTodayWidget(
          layanan: _con.layanan,
          con: _con,
          heroTag: 'home_top_restaurants'),
          ),
        ]),
        ),
      ),
    );
  }

}

class DynamicDialog extends StatefulWidget {
  DynamicDialog();

  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  File images;
  GlobalKey<FormState> addProducFormKey;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      images = image;
    });
  }

  Future getImageCam() async {
    var image1 = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      images = image1;
    });
  }

  String nama_layanan,harga;

  Future addProduct(BuildContext context) async {
    if (addProducFormKey.currentState.validate()) {
      if(images != null){
        addProducFormKey.currentState.save();

        var stream = new http.ByteStream(DelegatingStream.typed(images.openRead()));
        // get file length
        var length = await images.length();

        User _user = await getCurrentUser();

        Map<String, String> bod = {
          "nama_layanan":nama_layanan,
          "harga" : harga,
          "id_penyedia_jasa" : _user.idPenyediaJasa,
        };

        final String url = '${GlobalConfiguration().getString('base_url')}layanan/add_layanan';
        var request = new http.MultipartRequest("POST", Uri.parse(url));
        var multipartFile = new http.MultipartFile('image', stream, length,
            filename: basename(images.path));

        request.files.add(multipartFile);
        request.fields.addAll(bod);

        var response = await request.send();
        var responseq = await response.stream.bytesToString();
        print(responseq);
        response.stream.transform(utf8.decoder).listen((value) {
          print("upload");
          print(value);
          setState(() {
            HomeController f = new HomeController();
            f.refresh();
//            Navigator.of(context).pushNamed('/Pages', arguments: 2);
          });

        });


      }
    }
  }

  @override
  void initState() {
    super.initState();
    addProducFormKey = new GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Tambah Layanan")),
      content: Form(
        key: addProducFormKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  onSaved: (input) => nama_layanan = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Nama Layanan',
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Nama Layanan',
                    labelStyle: TextStyle(color: Colors.black),
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  onSaved: (input) => harga = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Harga Layanan',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Harga Layanan',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                            Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Text("Gambar Layanan"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.image),
                    tooltip: 'Galery',
                    onPressed: () {
                      setState(() {
                        getImage();
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    tooltip: 'Camera',
                    onPressed: () {
                      setState(() {
                        getImageCam();
                      });
                    },
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
//                child: images == null
//                    ? Text('No image selected.')
//                    : Image.file(images),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: images == null
                          ? Text('No image selected.',)
                          : Image.file(images,
                          height: 150.0,
                          width: 200.0,
                          fit: BoxFit.cover
                      ),
                    ),

                    SizedBox(height: 5),
                  ],
                ),
              ),
              FlatButton(
                padding: EdgeInsets.all(8),
                onPressed: () {
                  setState(() {
                    if (addProducFormKey.currentState.validate()) {
                      if (images != null ) {
                        addProduct(context);
                        Navigator.pop(context);
                        HomeController f = new HomeController();
                        f.refresh();
                        Navigator.of(context).pushNamed('/Pages', arguments: 2);
                      }else{
                        Fluttertoast.showToast(
                            msg: "Gambar Layanan Belum Dpilih",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                      }
                    }
                  });
                },
                child: Text("Upload"),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

