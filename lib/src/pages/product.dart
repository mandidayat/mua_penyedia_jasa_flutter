import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:restaurant_rlutter_ui/generated/i18n.dart';
import 'package:restaurant_rlutter_ui/src/controllers/product_controller.dart';
import 'package:restaurant_rlutter_ui/src/elements/ProductitemGridWidget.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';

class AllProductWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  AllProductWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ProductWidgetState createState() => _ProductWidgetState();
}

class _ProductWidgetState extends StateMVC<AllProductWidget> {
  ProductController _con;


  _ProductWidgetState() : super(ProductController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
            onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text(
            S.of(context).my_product,
            style: Theme.of(context)
                .textTheme
                .title
                .merge(TextStyle(letterSpacing: 1.3)),
          )),
      body: RefreshIndicator(
        onRefresh: _con.refreshOrders,
        child: GridView.count(
          crossAxisCount: 2,
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 17.0,
          childAspectRatio: 0.740,
          primary: false,
          children: List.generate(
            /// Get data in flashSaleItem.dart (ListItem folder)
            _con.product.length,
            (index) => ProductItemGrideWidget(
                heroTag: 'my_orders', modelAllProduct: _con.product[index]),
          ),
//              )
//            ],
        ),
//        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DynamicDialog();
              });
        },
        child: Icon(Icons.add),
        foregroundColor: Colors.white,
      ),
    );
  }
}
enum statProd { lafayette, ass}

class DynamicDialog extends StatefulWidget {
  DynamicDialog();

  @override
  _DynamicDialogState createState() => _DynamicDialogState();
}

class _DynamicDialogState extends State<DynamicDialog> {
  File images;
  GlobalKey<FormState> addProducFormKey;
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      images = image;
    });
  }

  Future getImageCam() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      images = image;
    });
  }

  String prod_title,parent,prod_status="1",product_description,price,mrp,qty,unit,store_id_login,stk_qty;

  Datum _currentUser;
  Datum _currentSub;

  List<Datum> _currentKategori;
  int _radioValue1 =1;

  statProd _character = statProd.lafayette;
  Future<List<Datum>> _fetchSubKategori(List<Datum> da) async {
    return da;
  }

  Future<List<Datum>> _fetchKategori() async {
    User _user = await getCurrentUser();
    var bodya = {
      "user_id":_user.idPenyediaJasa
    };

    final String url = '${GlobalConfiguration().getString('base_url')}index.php/api/get_categories';
    final client = new http.Client();
    final response = await client.post(
      url,
      body: bodya,
    );

    if (response.statusCode == 200) {
      return Users.fromJson(json.decode(response.body)).data;
    } else {
      throw Exception('Failed to load internet');
    }
  }

  Future addProduct(BuildContext context) async {
    if (addProducFormKey.currentState.validate()) {
      if(images != null){
        addProducFormKey.currentState.save();

        var stream = new http.ByteStream(DelegatingStream.typed(images.openRead()));
        // get file length
        var length = await images.length();

        User _user = await getCurrentUser();

        Map<String, String> bod = {
          "prod_title":prod_title,
          "parent" : parent,
          "prod_status" : prod_status,
          "product_description" : product_description,
          "price" : price,
          "mrp" :mrp,
          "qty" : qty,
          "qty" : qty,
          "unit" : unit,
          "store_id_login" : _user.idPenyediaJasa,
          "stk_qty" : stk_qty,
        };

        final String url = '${GlobalConfiguration().getString('base_url')}index.php/api/add_product';
        var request = new http.MultipartRequest("POST", Uri.parse(url));
        var multipartFile = new http.MultipartFile('prod_img', stream, length,
            filename: basename(images.path));

        request.files.add(multipartFile);
        request.fields.addAll(bod);

        var response = await request.send();
        var responseq = await response.stream.bytesToString();
        print(responseq);
        response.stream.transform(utf8.decoder).listen((value) {
          print(value);
          setState(() {
            ProductController f = new ProductController();
            f.refresh();
          });

        });


      }
    }
  }

  @override
  void initState() {
    super.initState();
    addProducFormKey = new GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Add new produk")),
      content: Form(
        key: addProducFormKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  onSaved: (input) => prod_title = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Nama Product',
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Nama Product',
                    labelStyle: TextStyle(color: Colors.black),
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              new Divider(
                height: 5.0,
                color: Colors.black,
              ),
              FutureBuilder<List<Datum>>(
                  future: _fetchKategori(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Datum>> snapshot) {
                    if (!snapshot.hasData) return CircularProgressIndicator();
                    return DropdownButton<Datum>(
                      items: snapshot.data
                          .map((user) => DropdownMenuItem<Datum>(
                        child: Text(user.title),
                        value: user,
                      ))
                          .toList(),
                      onChanged: (Datum value) {
                        setState(() {
                          _currentUser = value;
                          _currentKategori = value.subCat;
                        });
                      },
                      isExpanded: true,
                      //value: _currentUser,
                      hint: Text('Pilih Kategori'),
                    );
                  }),
              _currentUser != null
                  ? Column(children: <Widget>[
                Text( " - "   + _currentUser.title),
                FutureBuilder<List<Datum>>(
                    future: _fetchSubKategori(_currentKategori),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Datum>> snapshot) {
                      if (!snapshot.hasData) return Text("Kategori Belum Dipilih");
                      return DropdownButton<Datum>(
                        items: snapshot.data
                            .map((user) => DropdownMenuItem<Datum>(
                          child: Text(user.title),
                          value: user,
                        ))
                            .toList(),
                        onChanged: (Datum value) {
                          setState(() {
                            _currentSub = value;
                            parent = value.id;
//                          _currentKategori = value.subCat;

                          });
                        },
                        isExpanded: true,
//                          value: _currentUser,
                        hint: Text('Pilih Sub Kategori'),
                      );
                    }),
                  _currentSub != null ? Text(" - " + _currentSub.title) : Text("Sub Kategori Belum Dipilih"),
              ],)
                  : Text("Kategori Belum Dipilih"),


              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => product_description = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Deskripsi Produk',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Deskripsi Produk',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              new Divider(
                height: 5.0,
                color: Colors.black,
              ),
              Text("Gambar Produk"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.image),
                    tooltip: 'Galery',
                    onPressed: () {
                      setState(() {
                        getImage();
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    tooltip: 'Camera',
                    onPressed: () {
                      setState(() {
                        getImageCam();
                      });
                    },
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
//                child: images == null
//                    ? Text('No image selected.')
//                    : Image.file(images),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: images == null
                    ? Text('No image selected.',)
                    : Image.file(images,
                          height: 150.0,
                          width: 200.0,
                          fit: BoxFit.cover
                      ),
                    ),

                    SizedBox(height: 5),
                  ],
                ),
              ),
              new Divider(
                height: 5.0,
                color: Colors.black,
              ),
              Text("Status Produk"),
              ListTile(
                title: const Text('In Stok'),
                leading: Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: (value) {
                    setState(() {
                      _radioValue1 = value;
                      prod_status = "1";
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Deactive'),
                leading: Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: (value) {
                    setState(() {
                      prod_status = "0";
                      _radioValue1 = value;
                    });
                  },
                ),
              ),
              new Divider(
                height: 5.0,
                color: Colors.black,
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => mrp = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'MRP',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: '0.0',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => price = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Price',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: '0.0',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => qty = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Unit',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: '0.0',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => unit = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Unit Value',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'KG/BAG/NOS/QTY/etc',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                        onSaved: (input) => stk_qty = input.trim(),
                  validator: (input) => input.length < 1
                      ? S.of(context).should_be_a_valid_input
                      : null,
                  decoration: InputDecoration(
                    labelText: 'Stock Qty',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: '0.0',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                ),
              ),
              FlatButton(
                padding: EdgeInsets.all(8),
                onPressed: () {
                  setState(() {
                    if (addProducFormKey.currentState.validate()) {
                      if (images != null) {
                        addProduct(context);
                        Navigator.pop(context);
                        ProductController f = new ProductController();
                        f.refresh();
                        Navigator.of(context).pushNamed('/Pages', arguments: 5);
                      }
                    }
                  });
                },
                child: Text("Add Product"),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Users {

  bool responce;
  List<Datum> data;

  Users({
  this.responce,
  this.data,
  });

  factory Users.fromJson(Map<String, dynamic> json) => Users(
  responce: json["responce"],
  data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
  "responce": responce,
  "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };

}

class Datum {
  String id;
  String title;
  String arbTitle;
  String slug;
  String parent;
  String leval;
  String description;
  String image;
  String image2;
  String image2Status;
  String status;
  String storeIdLogin;
  String count;
  String pCount;
  List<Datum> subCat;

  Datum({
  this.id,
  this.title,
  this.arbTitle,
  this.slug,
  this.parent,
  this.leval,
  this.description,
  this.image,
  this.image2,
  this.image2Status,
  this.status,
  this.storeIdLogin,
  this.count,
  this.pCount,
  this.subCat,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
  id: json["id"],
  title: json["title"],
  arbTitle: json["arb_title"],
  slug: json["slug"],
  parent: json["parent"],
  leval: json["leval"],
  description: json["description"],
  image: json["image"],
  image2: json["image2"],
  image2Status: json["image2_status"],
  status: json["status"],
  storeIdLogin: json["store_id_login"],
  count: json["Count"],
  pCount: json["PCount"],
  subCat: json["sub_cat"] == null ? null : List<Datum>.from(json["sub_cat"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
  "id": id,
  "title": title,
  "arb_title": arbTitle,
  "slug": slug,
  "parent": parent,
  "leval": leval,
  "description": description,
  "image": image,
  "image2": image2,
  "image2_status": image2Status,
  "status": status,
  "store_id_login": storeIdLogin,
  "Count": count,
  "PCount": pCount,
  "sub_cat": subCat == null ? null : List<dynamic>.from(subCat.map((x) => x.toJson())),
  };

}