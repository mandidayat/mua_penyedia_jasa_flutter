import 'dart:convert';

import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_rlutter_ui/src/models/Layanan_model.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';


Future<LayananModel> getOrderToday() async {
  User _user = await getCurrentUser();

  var bodya = {
    "id_penyedia_jasa":_user.idPenyediaJasa
  };
  print("respon");
  final String url = '${GlobalConfiguration().getString('base_url')}layanan/layanan';
  final client = new http.Client();
  final response = await client.post(
      url,
    body: bodya,
  );

  if (response.statusCode == 200) {

  }
  print(response.body);
  return layananModelFromJson(response.body);
}


Future cancelOrder(String id) async {
  print("confirm order");
  var bodya = {
    "sale_id":id
  };

  final String url = '${GlobalConfiguration().getString('base_url_grocery')}index.php/api/cancel_by_store';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: bodya,
  );
  print(response.body);
  if (response.statusCode == 200)
    return json.decode(response.body);
}

Future confirOrder(String id) async {
  print("confirm order");
  var bodya = {
    "sale_id":id
  };

  final String url = '${GlobalConfiguration().getString('base_url_grocery')}index.php/api/confirm_by_store';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: bodya,
  );
  print(response.body);
  if (response.statusCode == 200)
    return json.decode(response.body);
}

Future confirPickUP(String id) async {
  print("pickup order");
  var bodya = {
    "sale_id":id
  };

  final String url = '${GlobalConfiguration().getString('base_url_grocery')}index.php/api/rtp_by_store';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: bodya,
  );
  print(response.body);
  if (response.statusCode == 200)
    return json.decode(response.body);
}