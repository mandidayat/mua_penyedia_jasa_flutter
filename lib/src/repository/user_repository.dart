import 'dart:convert';

import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

User currentUser = new User();


Future<User> login(User user) async {

  final String url = '${GlobalConfiguration().getString('base_url')}login/login_penyedia_jasa';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: user.toMap(),
  );
  if (response.statusCode == 200) {
    setCurrentUser(response.body);
    print("Respon Body");
    print(json.decode(response.body)['data']);
    currentUser = User.fromJSON(json.decode(response.body)['data'][0]);
    print(currentUser.nama);
  }
  return currentUser;
}

Future register(User _user) async {
  var bodya = {
    "username":_user.username,
    "password":_user.password,
    "nama":_user.nama,
    "alamat":_user.alamat,
    "no_hp":_user.noHp,
  };

  final String url = '${GlobalConfiguration().getString('base_url')}register/registrasi_penyedia_jasa';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: bodya,
  );
  if (response.statusCode == 200) {

  }
  return response.body;
}

Future<void> logout() async {
  currentUser = new User();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('current_user');
}

void setCurrentUser(jsonString) async {
  if (json.decode(jsonString)['data'][0] != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_user', json.encode(json.decode(jsonString)['data'][0]));
  }
}

Future<User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('current_user')) {
    currentUser = User.fromJSON(json.decode(await prefs.get('current_user')));
    print("getCurrentUser : " + currentUser.nama);
    print("getToken : " + currentUser.idPenyediaJasa);
  }
  return currentUser;
}

Future<User> update(User _user) async {
  var bodya = {
    "id_penyedia_jasa":_user.idPenyediaJasa,
    "nama":_user.nama,
    "alamat":_user.alamat,
    "no_hp":_user.noHp,
  };

  final String url = '${GlobalConfiguration().getString('base_url')}login/update_profile_penyedia_jasa';
  final client = new http.Client();
  final response = await client.post(
    url,
    body: bodya,
  );
  if (response.statusCode == 200) {

  }

  setCurrentUser(response.body);
  currentUser = User.fromJSON(json.decode(response.body)['data'][0]);
  return currentUser;
}
