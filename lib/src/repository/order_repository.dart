import 'dart:convert';
import 'dart:io';

import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_rlutter_ui/src/models/model_my_order.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';


Future<List<MyOrders>> getMyOrder() async {
  User _user = await getCurrentUser();

  var bodya = {
    "user_id":_user.idPenyediaJasa
  };
  final String url = '${GlobalConfiguration().getString('base_url')}order/order?id_penyedia_jasa='+ _user.idPenyediaJasa;
  final client = new http.Client();
  final response = await client.get(url,);

  if (response.statusCode == 200) {

  }

  print("respon order");
  print(json.decode(response.body));
  return myOrdersFromJson(response.body);
}

