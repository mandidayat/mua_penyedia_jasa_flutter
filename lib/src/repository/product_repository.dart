import 'dart:convert';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_rlutter_ui/src/models/model_all_product.dart';
import 'package:restaurant_rlutter_ui/src/models/user.dart';
import 'package:restaurant_rlutter_ui/src/repository/user_repository.dart';


Future<List<ModelAllProduct>> getMyProduct() async {
  User _user = await getCurrentUser();

  var bodya = {
    "user_id":_user.idPenyediaJasa
  };
  final String url = '${GlobalConfiguration().getString('base_url')}index.php/api/layanan_get';
  final client = new http.Client();
  final response = await client.post(
    url,
//    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: bodya,
  );
  if (response.statusCode == 200) {

  }

  print("respon product");
  print(json.decode(response.body));
  return modelAllProductFromJson(response.body);
}


